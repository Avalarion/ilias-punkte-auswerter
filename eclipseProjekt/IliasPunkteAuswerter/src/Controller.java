import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * Main Controller Class
 * 
 * @author Bastian Bringenebrg <spam@bastian-bringenberg.de>
 * @version 1.0.0
 */
public class Controller {
	File userList;
	File scoreList;
	File outputFile;
	
	/**
	 * Fake Constructor to pass 2 Arguments
	 * 
	 * @param userList
	 * @param scoreList
	 */
	public Controller(String userList, String scoreList) {
		this(userList, scoreList, "./output");
	}

	/**
	 * Main Constructor
	 * 
	 * @param userList
	 * @param scoreList
	 * @param outputFile
	 */
	public Controller(String userList, String scoreList, String outputFile) {
		this.userList = new File(userList);
		this.scoreList = new File(scoreList);
		this.outputFile = new File(outputFile);
	}

	/**
	 * Main Method renders right usage of parameters and calls Constructors
	 * 
	 * @param args
	 */
	public static void main(String[] args){
		Controller ctrl = null;
		if(args.length == 2 || args.length ==3){
			if(args.length == 2){
				ctrl = new Controller(args[0], args[1]);
			}else{
				ctrl = new Controller(args[0], args[1], args[2]);
			}
		}else{
			System.err.println("Ihre Eingabe ist Fehlerhaft, beachten Sie:");
			System.out.println("1. Parameter: Nutzerliste (.xls)");
			System.out.println("2. Parameter: Punkteliste (.xls)");
			System.out.println("3. Parameter: Ausgabedatei (.sql) [Optional]");
			System.exit(-1);
		}
		ctrl.doCommandLineInterface();
	}
	
	/**
	 * A basic GUI less CommandLine using solution
	 * 
	 */
	public void doCommandLineInterface(){
		if(!this.userList.exists()){
			System.err.println("Nutzerliste existiert nicht!");
			System.exit(-1);
		}
		if(!this.scoreList.exists()){
			System.err.println("Punkteliste existiert nicht!");
			System.exit(-1);
		}
		if(this.outputFile.exists()){
			System.err.print("Ausgabedateil existiert schon. überschreiben? (Y/n)");
			InputStreamReader converter = new InputStreamReader(System.in);
			BufferedReader in = new BufferedReader(converter);
			String readLine ="";
			try {
				readLine = in.readLine();
			} catch (IOException e) {}
			readLine = readLine.trim().toLowerCase();
			if(readLine.equals("y") || readLine.isEmpty()){
				System.out.println("Datei wird gelöscht.");
				this.outputFile.delete();
			}else{
				System.err.println("Datei wird nicht gelöscht.");
				System.exit(-1);
			}
		}
		try {
			Exporter.makeCSV(Mitglied.readScoreList(scoreList, Mitglied.readUserlist(userList)),outputFile);
		} catch (FileNotFoundException e) {
			System.err.println("Ausgabe Datei konnte nicht geöffnet werden!");
		}
	}
}