import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.Collection;
import java.util.Map;

/**
 * Exporter Collection
 * 
 * @author Bastian Bringenebrg <spam@bastian-bringenberg.de>
 * @version 1.0.0
 */
public class Exporter {
	
	/**
	 * Basic CSV Exporter
	 * 
	 * @param readScoreList
	 * @param output
	 * @throws FileNotFoundException
	 */
	public static void makeCSV(Map<String, Mitglied> readScoreList, File output) throws FileNotFoundException {
		Collection<Mitglied> members = readScoreList.values();
		PrintWriter pw = new PrintWriter(output);	
		for(Mitglied member: members){
			String line = member.getUid()+";"+member.getName()+";"+member.getScoreResult();
			pw.println(line);
		}
		pw.close();
	}

}
