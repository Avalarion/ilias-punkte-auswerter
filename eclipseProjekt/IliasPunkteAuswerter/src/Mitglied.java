import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import jxl.*;
import jxl.read.biff.BiffException;

/**
 * Mitglieder Rendering
 * 
 * @author Bastian Bringenebrg <spam@bastian-bringenberg.de>
 * @version 1.0.0
 */
public class Mitglied {
	private final static int iliasColumn = 1;
	private final static int firstNameColumn = 2;
	private final static int lastNameColumn = 3;
	private final static int uidColumn = 4;
	private final static double maxScore = 15.0;
	private final static boolean DEBUG = false;
	
	private String name;
	private String iliasName;
	private String uid;
	private double scoreResult;
	private ArrayList<Double> scoreTable = new ArrayList<Double>();
	
	/**
	 * Mitglied constructor
	 * 
	 * @param name
	 * @param iliasName
	 * @param uid
	 */
	public Mitglied(String name, String iliasName, String uid){
		this.name = name;
		this.iliasName = iliasName;
		this.uid = uid;
	}
	
	/**
	 * Reads the UserList and creates a Map with Users
	 * 
	 * @param userList
	 * @return the Map with Users
	 */
	public static Map<String, Mitglied> readUserlist(File userList){
		Workbook w;
		HashMap<String, Mitglied> members = new HashMap<String, Mitglied>();
		try {
			w = Workbook.getWorkbook(userList);
			Sheet sheet = w.getSheet(0);
			// Start at 1 because Title line is not needed
			for (int i = 1; i < sheet.getRows(); i++) {
				Mitglied member = new Mitglied(sheet.getCell(firstNameColumn, i).getContents()+" "+sheet.getCell(lastNameColumn, i).getContents(),sheet.getCell(iliasColumn, i).getContents(),sheet.getCell(uidColumn, i).getContents());
				if(DEBUG) System.out.println("Nutzer gefunden: " + member);
				members.put(member.getIliasName(), member);
			}
		} catch (BiffException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return members;
	}
	
	/**
	 * Reads the ScoreList and adds score to matched members
	 * 
	 * @param scoreList
	 * @param userlist
	 * 
	 * @return the Map with Users
	 */
	public static Map<String, Mitglied> readScoreList(File scoreList, Map<String, Mitglied> userlist){
		Workbook w;
		try {
			w = Workbook.getWorkbook(scoreList);
			Sheet sheet = w.getSheet(1);
			// Start at 1 because Title line is not needed
			for (int i = 1; i < sheet.getRows(); i++) {
				String user=sheet.getCell(0, i).getContents();
				user = user.split("\\[")[1].split("\\]")[0];
				if(userlist.containsKey(user)){
					Mitglied curUser = userlist.get(user);
					if(DEBUG) System.out.println("Nutzer gefunden: "+user);
					for(int j = 1; j<sheet.getColumns(); j++){
						if(!sheet.getCell(j, i).getContents().isEmpty()){
							String tmp = sheet.getCell(j, i).getContents();
							tmp = tmp.replace(",", ".");
							try{
								Double punkte = Double.parseDouble(tmp);
								curUser.getScoreTable().add(punkte);
								if(DEBUG) System.out.print(punkte+" - ");
							}catch(Exception e){
								System.err.println("Punkte für "+curUser.iliasName+" konnten nicht eingetragen werden. In Spalte: "+j+"; String: "+tmp);
							}
						}
					}
					curUser.calcScore();
					if(DEBUG) System.out.println("Gesamt: "+curUser.getScoreResult());
				}else{
					System.err.println("Nutzer nicht gefunden: "+user);
				}
			}
		} catch (BiffException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return userlist;
	}
	
	/**
	 * Calculate the scoreResult out of the scoreTable
	 * 
	 */
	public void calcScore() {
		Double score = 0.0;
		for(int i = 0; i < this.scoreTable.size(); i++){
			score += this.scoreTable.get(i);
		}
		if(score > maxScore) score = maxScore;
		this.scoreResult = score;
	}

	/**
	 * Gets the Name
	 * 
	 * @return
	 */
	public String getName() {
		return name;
	}
	
	/**
	 * Gets the iliasName
	 * 
	 * @return
	 */
	public String getIliasName() {
		return iliasName;
	}

	/**
	 * Gets the UID
	 * 
	 * @return
	 */
	public String getUid() {
		return uid;
	}

	/**
	 * Gets the Score Result
	 * 
	 * @return
	 */
	public double getScoreResult() {
		return scoreResult;
	}

	/**
	 * Gets the Score Table
	 * 
	 * @return
	 */
	public ArrayList<Double> getScoreTable() {
		return scoreTable;
	}
	
	/**
	 * Generates Default String Output
	 * 
	 */
	public String toString(){
		return this.name+": "+this.uid+", "+this.iliasName;
	}
}
